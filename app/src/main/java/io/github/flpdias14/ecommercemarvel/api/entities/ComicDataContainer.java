package io.github.flpdias14.ecommercemarvel.api.entities;

import java.util.List;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class ComicDataContainer {

    private int offset;
    private int limit;
    private int total;
    private int count;
    private List<Comic> results;

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    public int getTotal() {
        return total;
    }

    public int getCount() {
        return count;
    }

    public List<Comic> getResults() {
        return results;
    }
}
