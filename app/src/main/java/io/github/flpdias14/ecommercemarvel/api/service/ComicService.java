package io.github.flpdias14.ecommercemarvel.api.service;

import java.util.Map;

import io.github.flpdias14.ecommercemarvel.api.entities.ComicDataWrapper;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public interface ComicService {

    @GET("v1/public/comics")
    Call<ComicDataWrapper> getComics(@QueryMap Map<String, String> params);
}
