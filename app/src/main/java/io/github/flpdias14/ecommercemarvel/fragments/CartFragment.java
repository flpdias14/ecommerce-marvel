package io.github.flpdias14.ecommercemarvel.fragments;

import static io.github.flpdias14.ecommercemarvel.enums.DiscountCodesEnum.COMMON;
import static io.github.flpdias14.ecommercemarvel.enums.DiscountCodesEnum.RARE;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.github.flpdias14.ecommercemarvel.R;
import io.github.flpdias14.ecommercemarvel.adapters.ItemCartAdapter;
import io.github.flpdias14.ecommercemarvel.enums.DiscountCodesEnum;
import io.github.flpdias14.ecommercemarvel.exeptions.InvalidDiscountExeption;
import io.github.flpdias14.ecommercemarvel.model.CartItem;
import io.github.flpdias14.ecommercemarvel.repositories.CartItemRepository;

/**
 * Created by Felipe Oliveira on 10/10/2021
 */
public class CartFragment extends Fragment {

    private RecyclerView mRecyclerView;


    private EditText mDiscountEditText;
    private TextView mSubtotalTextView;
    private TextView mDiscountTextView;
    private TextView mTotalTextView;

    private Button mApplyDiscountButton;
    private Button mCheckoutButton;
    private double mSubtotal;
    private double mSubtotalCommon;
    private double mDiscount;
    private double mTotal;

    public CartFragment(){
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        mRecyclerView = view.findViewById(R.id.c_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mSubtotalTextView = view.findViewById(R.id.c_subtotal_descriptive_tv);
        mDiscountTextView = view.findViewById(R.id.c_discount_applied_descriptive_tv);
        mTotalTextView = view.findViewById(R.id.c_total_descriptive_tv);

        mDiscountEditText = view.findViewById(R.id.c_discount_coupon_et);

        mApplyDiscountButton = view.findViewById(R.id.c_apply_btn);
        mCheckoutButton = view.findViewById(R.id.c_checkout_btn);

        ItemCartAdapter adapter = new ItemCartAdapter(CartItemRepository
                .getInstance().getCartItems().toArray(new CartItem[0])
        );
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        // calculate the subtotal
        for (CartItem cartItem : CartItemRepository.getInstance().getCartItems()){
            mSubtotal += cartItem.getQuantity() * cartItem.getComic().getPrice();
            if(!cartItem.getComic().isRare()){
                mSubtotalCommon += cartItem.getQuantity() * cartItem.getComic().getPrice();
            }
        }
        // setting the total equal to subtotal
        mTotal = mSubtotal;
        mSubtotalTextView.setText(String.format("$%.2f", mSubtotal));
        mDiscountTextView.setText(String.format("$%.2f", mDiscount));
        mTotalTextView.setText(String.format("$%.2f", mTotal));

        // appling the discount coupon
        mApplyDiscountButton.setOnClickListener(view12 -> {
            try {
                validate();
                if(mDiscountEditText.getText().toString().trim().equals(RARE.getValue())){
                    applyDiscountRare();

                }
                else if (mDiscountEditText.getText().toString().trim().equals(COMMON.getValue())){
                    applyDiscountCommon();
                }
            } catch (InvalidDiscountExeption invalidDiscountExeption) {
                // coupon invalid
                Toast.makeText(getContext(),
                        invalidDiscountExeption.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        // order finish
        mCheckoutButton.setOnClickListener(view1 -> {
            CartItemRepository.getInstance().getCartItems().clear();
            clearScreen(view);
            Toast.makeText(getContext(), getString(R.string.order_finish), Toast.LENGTH_LONG).show();
        });
        return view;
    }

    /**
     * Clear all views of a screen.
     * @param view
     */
    private void clearScreen(View view) {
        mRecyclerView.setVisibility(View.INVISIBLE);
        mSubtotalTextView.setVisibility(View.INVISIBLE);
        mTotalTextView.setVisibility(View.INVISIBLE);
        mDiscountTextView.setVisibility(View.INVISIBLE);
        TextView discoutCoupon = view.findViewById(R.id.c_discount_coupon_tv);
        discoutCoupon.setVisibility(View.INVISIBLE);
        mDiscountEditText.setVisibility(View.INVISIBLE);
        mCheckoutButton.setVisibility(View.INVISIBLE);
        mApplyDiscountButton.setVisibility(View.INVISIBLE);
        TextView subtotal = view.findViewById(R.id.c_subtotal_tv);
        subtotal.setVisibility(View.INVISIBLE);
        TextView discount = view.findViewById(R.id.c_discount_applied_tv);
        discount.setVisibility(View.INVISIBLE);
        TextView total = view.findViewById(R.id.c_total_tv);
        total.setVisibility(View.INVISIBLE);

        TextView empty = view.findViewById(R.id.c_cart_empty);
        empty.setVisibility(View.VISIBLE);
    }

    /**
     * Calculate and apply the common discount
     */
    private void applyDiscountCommon() {
        mTotal = mSubtotal - mSubtotalCommon * COMMON.getDiscount();
        updateDiscount();
    }

    /**
     * Validate if the coupon is valid
     * @throws InvalidDiscountExeption
     */
    private void validate() throws InvalidDiscountExeption {

        if (!mDiscountEditText.getText().toString().trim().equals("")
                &&
                !mDiscountEditText.getText().toString().trim().equals("RARO")
                &&
                !mDiscountEditText.getText().toString().equals("COMUM")){
            throw new InvalidDiscountExeption();
        }

    }

    /**
     * Calculate and apply the rare coupon
     */
    @SuppressLint("DefaultLocale")
    private void applyDiscountRare() {
        mTotal = mSubtotal - mSubtotal * RARE.getDiscount();
        updateDiscount();
    }

    /**
     * Update the view when a discount is applied
     */
    @SuppressLint("DefaultLocale")
    private void updateDiscount() {
        mDiscount = mSubtotal - mTotal;
        mDiscountTextView.setText(String.format("%.2f", mDiscount));
        mTotalTextView.setText(String.format("%.2f", mTotal));
    }

}
