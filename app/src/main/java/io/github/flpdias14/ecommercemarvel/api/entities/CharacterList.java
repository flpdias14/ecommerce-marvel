package io.github.flpdias14.ecommercemarvel.api.entities;

import java.util.List;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class CharacterList {
    private int available;
    private int returned;
    private String collectionURI;
    private List<CharacterSumary> items;

    public int getAvailable() {
        return available;
    }

    public int getReturned() {
        return returned;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public List<CharacterSumary> getItems() {
        return items;
    }
}
