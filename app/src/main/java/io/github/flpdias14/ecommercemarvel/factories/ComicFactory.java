package io.github.flpdias14.ecommercemarvel.factories;

import io.github.flpdias14.ecommercemarvel.model.Comic;

/**
 * Created by Felipe Oliveira on 10/10/2021
 */
public class ComicFactory {

    private ComicFactory() {}

    public static Comic makeComic(io.github.flpdias14.ecommercemarvel.api.entities.Comic comic){
        Comic newComic = new Comic();
        newComic.setId(comic.getId());
        newComic.setDigitalId(comic.getDigitalId());
        newComic.setTitle(comic.getTitle());
        newComic.setIssueNumber(comic.getIssueNumber());
        newComic.setDescription(comic.getDescription());
        newComic.setIsbn(comic.getIsbn());
        newComic.setFormat(comic.getFormat());
        newComic.setPageCount(comic.getPageCount());
        newComic.setPrice(comic.getPrices().get(0).getPrice());
        newComic.setThumbnailURLInfo(comic.getThumbnail());
        newComic.setImagesURLInfo(comic.getImages());
        newComic.setCreators(comic.getCreators());
        newComic.setLanguage("en-US");

        return newComic;
    }
}
