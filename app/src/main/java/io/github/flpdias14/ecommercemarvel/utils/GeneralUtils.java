package io.github.flpdias14.ecommercemarvel.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class GeneralUtils {

    /**
     * Returns the md5 hash from a string
     * @param inputString string to hash
     * @return a hash from string
     * @throws NoSuchAlgorithmException .
     */
    public static String getMD5Hex(final String inputString) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(inputString.getBytes());

        byte[] digest = md.digest();

        return convertByteToHex(digest);
    }

    /**
     * Converts byte to a string representation of a hexadecimal
     * @param byteData byte array
     * @return string
     */
    private static String convertByteToHex(byte[] byteData) {

        StringBuilder sb = new StringBuilder();
        for (byte byteDatum : byteData) {
            sb.append(Integer.toString((byteDatum & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
