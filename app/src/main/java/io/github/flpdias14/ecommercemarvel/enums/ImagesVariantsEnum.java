package io.github.flpdias14.ecommercemarvel.enums;

/**
 * Created by Felipe Oliveira on 09/10/2021
 */
public enum ImagesVariantsEnum {

    /**
     * Portrait aspect ratio
     */
    PORTRAIT_SMALL_50X75_PX("portrait_small"),
    PORTRAIT_MEDIUM_100X150_PX("portrait_medium"),
    PORTRAIT_XLARGE_150X225_PX("portrait_xlarge"),
    PORTRAIT_FANTASTIC_168X252_PX("portrait_fantastic"),
    PORTRAIT_UNCANNY_300X450_PX("portrait_uncanny"),
    PORTRAIT_INCREDIBLE_256X324_PX("portrait_uncanny"),

    /**
     * Standard (square) aspect ratio
     */
    STANDARD_SMALL_65X45_PX("standard_small"),
    STANDARD_MEDIUM_100X100_PX("standard_medium"),
    STANDARD_LARGE_140X140_PX("standard_large"),
    STANDARD_XLARGE_200X200_PX("standard_xlarge"),
    STANDARD_FANTASTIC_250X250_PX("standard_fantastic"),
    STANDARD_AMAZING_180X180_PX("standard_amazing"),

    /**
     * Landscape aspect ratio
     */
    LANDSCAPE_SMALL_120X90_PX("landscape_small"),
    LANDSCAPE_MEDIUM_175x130_PX("landscape_medium"),
    LANDSCAPE_LARGE_190x140_PX("landscape_large"),
    LANDSCAPE_XLARGE_270x200_PX("landscape_xlarge"),
    LANDSCAPE_AMAZING_250x156_PX("landscape_amazing"),
    LANDSCAPE_INCREDIBLE_464x261_PX("landscape_amazing"),


    /**
     * Full size images
     */

    DETAIL("detail");



    private String value;

    ImagesVariantsEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}