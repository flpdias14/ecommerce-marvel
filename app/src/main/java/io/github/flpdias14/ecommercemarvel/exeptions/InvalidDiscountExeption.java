package io.github.flpdias14.ecommercemarvel.exeptions;

/**
 * Created by Felipe Oliveira on 12/10/2021
 */
public class InvalidDiscountExeption extends DefaultExeption{

    public InvalidDiscountExeption(){
        super("Error discount code invalid.");
        this.message ="Error discount code invalid.";
    }

    public InvalidDiscountExeption(String message){
        super(message);
        this.message = message;
    }
}
