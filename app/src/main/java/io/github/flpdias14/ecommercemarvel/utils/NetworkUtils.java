package io.github.flpdias14.ecommercemarvel.utils;

import android.graphics.drawable.Drawable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Felipe Oliveira on 09/10/2021
 */
public class NetworkUtils {

    public static String formatUrlImages(String base, String format, String extension){
        return String.format("%s/%s.%s", base, format, extension);
    }
}
