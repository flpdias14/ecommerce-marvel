package io.github.flpdias14.ecommercemarvel.tasks;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Felipe Oliveira on 09/10/2021
 */
public class RetrieveImageTask extends AsyncTask<String, Void, Drawable> {

    InputStream inputStream = null;

    public interface AsyncResponse{
        void processFinish(Drawable drawable);
    }

    public AsyncResponse delegate;

    public RetrieveImageTask(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected Drawable doInBackground(String... urls) {

        try {
            inputStream = (InputStream) new URL(urls[0]).getContent();
            return Drawable.createFromStream(inputStream, "");

        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Drawable drawable) {
        delegate.processFinish(drawable);
    }
}
