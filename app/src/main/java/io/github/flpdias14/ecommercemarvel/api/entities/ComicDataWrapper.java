package io.github.flpdias14.ecommercemarvel.api.entities;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class ComicDataWrapper {

    private int code;
    private String status;
    private ComicDataContainer data;

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public ComicDataContainer getData() {
        return data;
    }
}
