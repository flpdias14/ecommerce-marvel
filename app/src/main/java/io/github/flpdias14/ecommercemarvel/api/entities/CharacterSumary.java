package io.github.flpdias14.ecommercemarvel.api.entities;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class CharacterSumary {

    private String resourceURI;
    private String name;
    private String role;

    public String getResourceURI() {
        return resourceURI;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }
}
