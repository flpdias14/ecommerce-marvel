package io.github.flpdias14.ecommercemarvel.api.entities;

import java.util.List;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class CreatorList {

    private int available;
    private int returned;
    private String collectionURI;
    private List<CreatorSumary> items;

    public int getAvailable() {
        return available;
    }

    public int getReturned() {
        return returned;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public List<CreatorSumary> getItems() {
        return items;
    }
}
