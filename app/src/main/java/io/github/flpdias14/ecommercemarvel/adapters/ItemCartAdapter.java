package io.github.flpdias14.ecommercemarvel.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import io.github.flpdias14.ecommercemarvel.R;
import io.github.flpdias14.ecommercemarvel.model.CartItem;

/**
 * Created by Felipe Oliveira on 11/10/2021
 */
public class ItemCartAdapter extends RecyclerView.Adapter<ItemCartAdapter.ViewHolder> {

    private final String TAG = "ic-debug";
    private CartItem[] mLocalDataSet;


    public ItemCartAdapter(CartItem[] mLocalDataSet) {
        this.mLocalDataSet = mLocalDataSet;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView textViewTitle;
        private final TextView textViewPrice;
        private final TextView textViewQuantity;
        private final ImageView imageViewThumbnail;
        private final ImageView imageViewAdd;
        private final ImageView imageViewRemove;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.rv_cart_item_tv);
            textViewPrice = itemView.findViewById(R.id.rv_cart_item_price_tv);
            textViewQuantity = itemView.findViewById(R.id.rv_cart_item_quantity_tv);
            imageViewThumbnail = itemView.findViewById(R.id.rv_cart_item_thumbnail_img);
            imageViewAdd = itemView.findViewById(R.id.rv_cart_item_add_img);
            imageViewRemove = itemView.findViewById(R.id.rv_cart_item_remove_img);
        }

        public TextView getTextViewTitle() {
            return textViewTitle;
        }

        public TextView getTextViewPrice() {
            return textViewPrice;
        }

        public ImageView getImageViewThumbnail() {
            return imageViewThumbnail;
        }

        public ImageView getImageViewAdd() {
            return imageViewAdd;
        }

        public ImageView getImageViewRemove() {
            return imageViewRemove;
        }

        public TextView getTextViewQuantity() {
            return textViewQuantity;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_row_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.getImageViewThumbnail().setImageDrawable(mLocalDataSet[position].getComic().getThumbnail());
        holder.getTextViewTitle().setText(mLocalDataSet[position].getComic().getTitle());
        holder.getTextViewPrice().setText(mLocalDataSet[position].getComic().getPrice() > 0 ?
                 String.format("$%.2f", mLocalDataSet[position].getComic().getPrice()) : "Free"
        );

        holder.getTextViewQuantity().setText(String.format("%d", mLocalDataSet[position].getQuantity()));

        holder.getImageViewAdd().setOnClickListener(view -> {
            mLocalDataSet[position].setQuantity(mLocalDataSet[position].getQuantity()+1);
            holder.getTextViewQuantity().setText(String.format("%d", mLocalDataSet[position].getQuantity()));
        });

        holder.getImageViewRemove().setOnClickListener(view -> {
            mLocalDataSet[position].setQuantity(mLocalDataSet[position].getQuantity()-1);
            holder.getTextViewQuantity().setText(String.format("%d", mLocalDataSet[position].getQuantity()));
        });

    }

    @Override
    public int getItemCount() {
        return mLocalDataSet.length;
    }

}
