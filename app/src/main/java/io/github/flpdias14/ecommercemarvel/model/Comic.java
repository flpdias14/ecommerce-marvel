package io.github.flpdias14.ecommercemarvel.model;

import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import io.github.flpdias14.ecommercemarvel.api.entities.CreatorList;
import io.github.flpdias14.ecommercemarvel.api.entities.CreatorSumary;
import io.github.flpdias14.ecommercemarvel.api.entities.Image;


/**
 * Created by Felipe Oliveira on 10/10/2021
 */
public class Comic {

    private int id;
    private int digitalId;
    private String title;
    private double issueNumber;
    private String description;
    private String isbn;
    private String format;
    private int pageCount;
    private float price;
    private Drawable thumbnail;
    private Drawable thumbnailDetail;
    private Image thumbnailURLInfo;
    private List<Image> imagesURLInfo;
    private List<Drawable> images;
    private boolean isRare;
    private CreatorList creators;
    private String language;
//    private CharacterList characters;



    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public int getDigitalId() {
        return digitalId;
    }

    public double getIssueNumber() {
        return issueNumber;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getFormat() {
        return format;
    }

    public int getPageCount() {
        return pageCount;
    }

    public float getPrice() {
        return price;
    }

    public Drawable getThumbnail() {
        return thumbnail;
    }

    public List<Drawable> getImages() {
        return images;
    }

    public Image getThumbnailURLInfo() {
        return thumbnailURLInfo;
    }

    public String getLanguage() {
        return language;
    }

    public List<Image> getImagesURLInfo() {
        return imagesURLInfo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDigitalId(int digitalId) {
        this.digitalId = digitalId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIssueNumber(double issueNumber) {
        this.issueNumber = issueNumber;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setThumbnail(Drawable thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setThumbnailURLInfo(Image thumbnailURLInfo) {
        this.thumbnailURLInfo = thumbnailURLInfo;
    }

    public void setImagesURLInfo(List<Image> imagesURLInfo) {
        this.imagesURLInfo = imagesURLInfo;
    }

    public void setImages(List<Drawable> images) {
        this.images = images;
    }

    public boolean isRare() {
        return isRare;
    }

    public void setRare(boolean rare) {
        isRare = rare;
    }

    public Drawable getThumbnailDetail() {
        return thumbnailDetail;
    }

    public void setThumbnailDetail(Drawable thumbnailDetail) {
        this.thumbnailDetail = thumbnailDetail;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAuthors() {
        StringBuilder author = new StringBuilder();
        for(CreatorSumary creatorSumary: creators.getItems()){
            author.append(creatorSumary.getName());
            if((author.length() > 0) && creators.getItems().size() > 1){
                author.append("; ");
            }
        }

        return author.toString();
    }

    public void setCreators(CreatorList creators) {
        this.creators = creators;
    }


    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Comic comic = (Comic) obj;
        return this.getId() == comic.getId();
    }

}