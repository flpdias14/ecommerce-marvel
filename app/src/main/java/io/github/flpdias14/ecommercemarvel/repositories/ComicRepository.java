package io.github.flpdias14.ecommercemarvel.repositories;

import java.util.Map;

import io.github.flpdias14.ecommercemarvel.api.entities.ComicDataWrapper;
import io.github.flpdias14.ecommercemarvel.api.service.ComicService;
import retrofit2.Call;

/**
 * Created by Felipe Oliveira on 09/10/2021
 */
public class ComicRepository {

    private ComicService mComicService;


    public ComicRepository(ComicService comicService){
        this.mComicService = comicService;
    }

    public Call<ComicDataWrapper> getComics(Map<String, String> params){
        return mComicService.getComics(params);
    }
}
