package io.github.flpdias14.ecommercemarvel.enums;

/**
 * Created by Felipe Oliveira on 12/10/2021
 */
public enum DiscountCodesEnum {


    COMMON("COMUM", 0.10),
    RARE("RARO", 0.25);


    private String value;
    private double discount;

    DiscountCodesEnum(String value, double discount) {
        this.value = value;
        this.discount = discount;
    }

    public String getValue() {
        return value;
    }

    public double getDiscount() {
        return discount;
    }

}