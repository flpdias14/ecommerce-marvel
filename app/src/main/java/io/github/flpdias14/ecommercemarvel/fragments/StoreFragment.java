package io.github.flpdias14.ecommercemarvel.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import io.github.flpdias14.ecommercemarvel.R;
import io.github.flpdias14.ecommercemarvel.adapters.ItemStoreAdapter;
import io.github.flpdias14.ecommercemarvel.api.config.WebServiceConfig;
import io.github.flpdias14.ecommercemarvel.api.entities.Comic;
import io.github.flpdias14.ecommercemarvel.api.entities.ComicDataWrapper;
import io.github.flpdias14.ecommercemarvel.api.service.ComicService;
import io.github.flpdias14.ecommercemarvel.dialogs.LoadingDialog;
import io.github.flpdias14.ecommercemarvel.enums.ImagesVariantsEnum;
import io.github.flpdias14.ecommercemarvel.factories.ComicFactory;
import io.github.flpdias14.ecommercemarvel.repositories.ComicLocalRepository;
import io.github.flpdias14.ecommercemarvel.repositories.ComicRepository;
import io.github.flpdias14.ecommercemarvel.tasks.RetrieveImageTask;
import io.github.flpdias14.ecommercemarvel.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Felipe Oliveira on 10/10/2021
 */
public class StoreFragment extends Fragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener{

    private RecyclerView mRecyclerView;

    private int mOffset;
    private int mLimit;
    Map<String, String> mParams;
    private ItemStoreAdapter mAdapter;
    ComicRepository mComicRepository;
    private final String TAG = "sf-debug" ;
    public StoreFragment(){
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store, container, false);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        final LoadingDialog loadingDialog = new LoadingDialog(getActivity());

        SearchView searchView = view.findViewById(R.id.search);
        searchView.setOnQueryTextListener(StoreFragment.this);
        setParams();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(WebServiceConfig.getBaseURL())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
         mComicRepository = new ComicRepository(retrofit.create(ComicService.class));


        call(loadingDialog);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)){
                    mParams.put("offset", String.valueOf(mOffset+mLimit));
                    call(loadingDialog);
                    Objects.requireNonNull(mRecyclerView.getLayoutManager()).scrollToPosition(mOffset);
                }
            }
        });


        return view;
    }

    /**
     * Set parameters to search on the webservice
     */
    private void setParams() {
        mParams = new HashMap<>();
        mParams.put("ts", WebServiceConfig.getTimestamp());
        mParams.put("apikey", WebServiceConfig.getPublicKey());

        mParams.put("hash", WebServiceConfig.getHash());

        mParams.put("noVariants", "true");
        mParams.put("orderBy", "title");
        mParams.put("limit", "30");
    }

    /**
     * Make the request to the webservice
     * @param loadingDialog dialog to show that the data is loading
     */
    private void call(LoadingDialog loadingDialog) {
        loadingDialog.startLoadingDialog();
        Call<ComicDataWrapper> call =  mComicRepository.getComics(mParams);

        call.enqueue(new Callback<ComicDataWrapper>() {
            @Override
            public void onResponse(Call<ComicDataWrapper> call, Response<ComicDataWrapper> response) {

                if (response.isSuccessful()){
                    Log.d(TAG, "The request sucessful "+ response.code());
                    extractData(loadingDialog, response.body());
                }
            }

            @Override
            public void onFailure(Call<ComicDataWrapper> call, Throwable t) {
                Log.d(TAG, "The request fail "+ call.request());
                Toast.makeText(getContext(), getText(R.string.error_comunication), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Process the data retrieved from the webservice
     * @param loadingDialog dialog to show that the data is being processing
     * @param comicDataWrapper payload
     */
    private void extractData(LoadingDialog loadingDialog, ComicDataWrapper comicDataWrapper) {
        mOffset = comicDataWrapper.getData().getOffset();
        mLimit = comicDataWrapper.getData().getLimit();

        // obtain the retrieved data from the server
        List<Comic> comics = comicDataWrapper.getData().getResults();

        // Create each comic
        for(Comic comic: comics){
            ComicLocalRepository.getInstance().getmComics().add(ComicFactory.makeComic(comic));
        }
        // calculate the number of rare comics
        int numberRareComics = (int) Math.round(comics.size()*0.12);
        int countRares = 0;
        for (io.github.flpdias14.ecommercemarvel.model.Comic c:
                ComicLocalRepository.getInstance().getmComics()) {
            // formats the url to request the thumbnail image
            String urlThumbnail = NetworkUtils.formatUrlImages(
                    c.getThumbnailURLInfo().getPath(),
                    ImagesVariantsEnum.PORTRAIT_MEDIUM_100X150_PX.getValue(),
                    c.getThumbnailURLInfo().getExtension());

            // make the request async if the image is not in the dataset
            if (c.getThumbnail() == null) {
                new RetrieveImageTask(c::setThumbnail).execute(urlThumbnail);
            }
            if(countRares < numberRareComics ){
                c.setRare( ThreadLocalRandom.current().nextBoolean());
                if (c.isRare()){
                    countRares++;
                }
            }
        }
        // instantiating the adapter
        mAdapter = new ItemStoreAdapter(
                ComicLocalRepository.getInstance().getmComics().toArray(new io.github.flpdias14.ecommercemarvel.model.Comic[0]),
                        getParentFragmentManager());
        // wait five seconds then set the data on the recycler view
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            loadingDialog.dimissDialog();
            // seting the adapter on the recyclerView
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        }, 5000);
    }

    /**
     * Implementation of the search field
     * @param query text input by users
     * @return true
     */
    @SuppressLint("NotifyDataSetChanged")
    @Override
    public boolean onQueryTextSubmit(String query) {
        // set up the parameters to the search
        mParams.clear();
        setParams();
        mParams.put("titleStartsWith", query);
        mParams.put("offset", "0");
        ComicLocalRepository.getInstance().getmComics().clear();
        mAdapter.notifyDataSetChanged();
        call(new LoadingDialog(getActivity()));
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public boolean onClose() {
        mParams.clear();
        setParams();
        ComicLocalRepository.getInstance().getmComics().clear();
        mAdapter.notifyDataSetChanged();
        call(new LoadingDialog(getActivity()));
        return true;
    }
}
