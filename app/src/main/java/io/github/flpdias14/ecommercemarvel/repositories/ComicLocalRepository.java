package io.github.flpdias14.ecommercemarvel.repositories;

import android.os.Build;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.github.flpdias14.ecommercemarvel.model.Comic;

/**
 * Created by Felipe Oliveira on 12/10/2021
 */
public class ComicLocalRepository {


    private static ComicLocalRepository mComicLocalRepository;
    private List<Comic> mComics;

    private ComicLocalRepository() {
        mComics = new ArrayList<>();
    }

    public static ComicLocalRepository getInstance(){
        if (mComicLocalRepository == null) mComicLocalRepository = new ComicLocalRepository();
        return mComicLocalRepository;
    }

    public List<Comic> getmComics() {
        return mComics;
    }

    public void addComic(Comic comic){
        mComics.add(comic);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mComics = mComics.stream().distinct().collect(Collectors.toList());
        }
    }
}
