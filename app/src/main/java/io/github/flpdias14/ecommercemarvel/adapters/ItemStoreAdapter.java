package io.github.flpdias14.ecommercemarvel.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import io.github.flpdias14.ecommercemarvel.R;

import io.github.flpdias14.ecommercemarvel.dialogs.ComicDetailsDialog;
import io.github.flpdias14.ecommercemarvel.model.Comic;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class ItemStoreAdapter extends RecyclerView.Adapter<ItemStoreAdapter.ViewHolder> {

    private final String TAG = "iv-debug";
    private Comic[] mLocalDataSet;
    private final FragmentManager mFragmentManager;
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView textViewTitle;
        private final TextView textViewDescription;
        private final TextView textViewPrice;
        private final ImageView imageViewThumbnail;
        private final ImageView imageViewRare;
        private final CardView cardView;
        private final Context context;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.rv_cart_item_tv);
            textViewDescription = itemView.findViewById(R.id.rv_item_description_tv);
            textViewPrice = itemView.findViewById(R.id.rv_cart_item_price_tv);
            imageViewThumbnail = itemView.findViewById(R.id.rv_cart_item_thumbnail_img);
            imageViewRare = itemView.findViewById(R.id.rv_cart_item_add_img);
            cardView = itemView.findViewById(R.id.rv_store_card_view);
            context = itemView.getContext();
        }

        public TextView getTextViewTitle(){
            return textViewTitle;
        }

        public ImageView getImageViewThumbnail() {
            return imageViewThumbnail;
        }

        public TextView getTextViewDescription() {
            return textViewDescription;
        }

        public TextView getTextViewPrice() {
            return textViewPrice;
        }

        public ImageView getImageViewRare() {
            return imageViewRare;
        }

        public CardView getCardView() {
            return cardView;
        }

        public Context getContext() {
            return context;
        }
    }

    public ItemStoreAdapter(Comic[] localDataSet, FragmentManager fragmentManager){
        this.mLocalDataSet = localDataSet;
        this.mFragmentManager = fragmentManager;

    }

    /**
     * Create new item views
     * @param parent ViewGroup
     * @param viewType int
     * @return ViewHolder object
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_row_item, parent, false);
        return new ViewHolder(view);
    }

    /**
     * Replace the contents of a view
     * @param holder ViewHolder of the item
     * @param position position of the item
     */
    @SuppressLint({"WrongConstant", "DefaultLocale"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        // seting the title
        holder.getTextViewTitle().setText(mLocalDataSet[position].getTitle());
        // seting the price
        holder.getTextViewPrice().setText(
                // check if the comic is free
                mLocalDataSet[position].getPrice() > 0 ?
                String.format("$ %.2f",  mLocalDataSet[position].getPrice())
                        : "Free"
        );
        holder.getTextViewDescription().setText(mLocalDataSet[position].getDescription());

        // verifying if the description text can be justify
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            holder.getTextViewDescription().setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }

        holder.getImageViewThumbnail().setImageDrawable(mLocalDataSet[position].getThumbnail());

        holder.getImageViewRare().setVisibility((mLocalDataSet[position].isRare() ? View.VISIBLE: View.INVISIBLE));
        holder.getCardView().setOnClickListener(view -> ComicDetailsDialog.display(mFragmentManager, mLocalDataSet[position]));
    }

    /**
     * Return the size of the dataset
     * @return int number of elements of the dataset
     */
    @Override
    public int getItemCount() {
        return mLocalDataSet.length;
    }


}
