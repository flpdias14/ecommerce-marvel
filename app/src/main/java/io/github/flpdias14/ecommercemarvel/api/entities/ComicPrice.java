package io.github.flpdias14.ecommercemarvel.api.entities;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class ComicPrice {
    private String type;
    private float price;

    public String getType() {
        return type;
    }

    public float getPrice() {
        return price;
    }
}
