package io.github.flpdias14.ecommercemarvel.api.entities;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class Image {

    private String path;
    private String extension;

    public String getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }
}
