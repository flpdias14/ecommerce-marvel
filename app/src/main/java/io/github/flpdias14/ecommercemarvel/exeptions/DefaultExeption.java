package io.github.flpdias14.ecommercemarvel.exeptions;

import androidx.annotation.Nullable;

/**
 * Created by Felipe Oliveira on 12/10/2021
 */
public class DefaultExeption extends Exception{

    protected String message;

    public DefaultExeption(){
        super("Error");
        this.message = "Error";
    }

    public DefaultExeption(String message){
        super(message);
        this.message = message;
    }

    @Nullable
    @Override
    public String getMessage() {
        return message;
    }
}
