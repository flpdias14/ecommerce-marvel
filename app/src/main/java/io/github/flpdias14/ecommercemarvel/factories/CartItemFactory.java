package io.github.flpdias14.ecommercemarvel.factories;

import io.github.flpdias14.ecommercemarvel.model.CartItem;
import io.github.flpdias14.ecommercemarvel.model.Comic;

/**
 * Created by Felipe Oliveira on 12/10/2021
 */
public class CartItemFactory {
    private CartItemFactory() {}

    public static CartItem makeCartItem(Comic comic, int quantity){
        return new CartItem(comic, quantity);
    }
}
