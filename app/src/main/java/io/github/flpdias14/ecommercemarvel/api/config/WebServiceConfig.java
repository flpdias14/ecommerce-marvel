package io.github.flpdias14.ecommercemarvel.api.config;

/**
 * Created by Felipe Oliveira on 09/10/2021
 * Web service configuration file.
 */
public class WebServiceConfig {

    private static final String baseURL = "http://gateway.marvel.com/";

    private static final String publicKey = "ad459b6eb3c5df7b1215a42f2ac7e1b8";

    private static final String timestamp = "1";

    private static final String hash = "fc2ef722360c723aa849b91918b36eab";

    public static String getBaseURL() {
        return baseURL;
    }

    public static String getPublicKey() {
        return publicKey;
    }

    public static String getTimestamp() {
        return timestamp;
    }

    public static String getHash() {
        return hash;
    }

    private WebServiceConfig() {}

}
