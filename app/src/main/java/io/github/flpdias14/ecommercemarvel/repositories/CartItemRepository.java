package io.github.flpdias14.ecommercemarvel.repositories;

import java.util.ArrayList;
import java.util.List;

import io.github.flpdias14.ecommercemarvel.model.CartItem;
import io.github.flpdias14.ecommercemarvel.model.Comic;

/**
 * Created by Felipe Oliveira on 12/10/2021
 */
public class CartItemRepository {

    private static CartItemRepository mRepository;
    private List<CartItem> mCartItems;

    private CartItemRepository() {
        this.mCartItems = new ArrayList<>();
    }

    public static CartItemRepository getInstance(){
        if (mRepository == null) mRepository = new CartItemRepository();
        return mRepository;
    }

    public List<CartItem> getCartItems() {
        return mCartItems;
    }

    public void addItem(CartItem cartItem){
        mCartItems.add(cartItem);
    }

    public void removeItem(CartItem cartItem){
        mCartItems.remove(cartItem);
    }

    public void removeItem(int index){
        mCartItems.remove(index);
    }

    public void update(List<CartItem> cartItems){
        mCartItems.clear();
        this.mCartItems = cartItems;
    }

    public boolean hasElement(Comic comic){
        for (CartItem item: mCartItems){
            if (item.getComic().equals(comic)) return true;
        }
        return false;
    }

    public CartItem getItemByComic(Comic comic){
        for (CartItem item: mCartItems){
            if (item.getComic().equals(comic)) return item;
        }
        return null;
    }

    public void update(Comic comic, int quantity) {

        getItemByComic(comic).setQuantity(getItemByComic(comic).getQuantity()+quantity);
    }
}
