package io.github.flpdias14.ecommercemarvel.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Objects;

import io.github.flpdias14.ecommercemarvel.R;

import io.github.flpdias14.ecommercemarvel.fragments.CartFragment;
import io.github.flpdias14.ecommercemarvel.fragments.ProfileFragment;
import io.github.flpdias14.ecommercemarvel.fragments.StoreFragment;
import io.github.flpdias14.ecommercemarvel.repositories.CartItemRepository;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "ma-debug";
    private CartItemRepository mCartRepository;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCartRepository = CartItemRepository.getInstance();

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigationView);

        // inflate the fragment store
        bottomNavigationView.setOnNavigationItemSelectedListener(MainActivity.this);
        Fragment storeFragment = new StoreFragment();
        openFragment(storeFragment);
    }

    /**
     * Implementation of bottom navigation bar
     * @param item
     * @return
     */
    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_store: {
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.app_name);
                Fragment storeFragment = new StoreFragment();
                openFragment(storeFragment);
                break;
            }
            case R.id.navigation_cart: {
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.cart);
                Fragment cartFragment = new CartFragment();
                openFragment(cartFragment);
                break;
            }
            case R.id.navigation_profile: {
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.profile);
                Fragment profileFragment = new ProfileFragment();
                openFragment(profileFragment);
                break;
            }
        }

        return true;
    }

    private void openFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}