package io.github.flpdias14.ecommercemarvel.model;

import java.util.Objects;

/**
 * Created by Felipe Oliveira on 11/10/2021
 */
public class CartItem {

    private Comic comic;
    private int quantity;

    public CartItem(Comic comic, int quantity) {
        this.comic = comic;
        this.quantity = quantity;
    }

    public Comic getComic() {
        return comic;
    }

    public void setComic(Comic comic) {
        this.comic = comic;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItem cartItem = (CartItem) o;
        return comic.getId() == cartItem.comic.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(comic, quantity);
    }
}
