package io.github.flpdias14.ecommercemarvel.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import io.github.flpdias14.ecommercemarvel.R;
import io.github.flpdias14.ecommercemarvel.enums.ImagesVariantsEnum;
import io.github.flpdias14.ecommercemarvel.factories.CartItemFactory;
import io.github.flpdias14.ecommercemarvel.model.Comic;
import io.github.flpdias14.ecommercemarvel.repositories.CartItemRepository;
import io.github.flpdias14.ecommercemarvel.tasks.RetrieveImageTask;
import io.github.flpdias14.ecommercemarvel.utils.NetworkUtils;

/**
 * Created by Felipe Oliveira on 11/10/2021
 */
public class ComicDetailsDialog extends DialogFragment {

    public static final String TAG = "cdd_debug";
    private static Comic mComic;

    private Toolbar mToolbar;


    public static ComicDetailsDialog display(FragmentManager fragmentManager, Comic comic) {
        ComicDetailsDialog exampleDialog = new ComicDetailsDialog();
        mComic = comic;
        exampleDialog.show(fragmentManager, TAG);
        return exampleDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.details_dialog, container, false);

        mToolbar = view.findViewById(R.id.toolbar);

        return view;
    }

    @SuppressLint({"WrongConstant", "DefaultLocale"})
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // configuring the tooobar
        mToolbar.setNavigationOnClickListener(v -> dismiss());
        mToolbar.setTitle(R.string.comic_details);
        mToolbar.inflateMenu(R.menu.details);
        mToolbar.setOnMenuItemClickListener(item -> {
            dismiss();
            return true;
        });

        ImageView thumbnail = view.findViewById(R.id.cd_thumbnail_img);
        ProgressBar progressBar = view.findViewById(R.id.cd_progressbar);
        TextView loadingImageTextView = view.findViewById(R.id.cd_loading_tv);
        TextView priceTextView = view.findViewById(R.id.cd_price_descriptive_tv);
        TextView titleTextView = view.findViewById(R.id.cd_title_descriptive_tv);
        TextView descriptionTextView = view.findViewById(R.id.cd_description_descriptive_tv);
        TextView languageTextView = view.findViewById(R.id.cd_language_descriptive_tv);
        TextView authorsTextView = view.findViewById(R.id.cd_authors_descriptive_tv);
        TextView formatTextView = view.findViewById(R.id.cd_format_descriptive_tv);
        TextView pagesTitleTextView = view.findViewById(R.id.cd_pages_tv);
        TextView pagesTextView = view.findViewById(R.id.cd_pages_descriptive_tv);
        TextView typeTextView = view.findViewById(R.id.cd_type_descriptive_tv);
        TextView quantityTextView = view.findViewById(R.id.cd_quantity_descriptive_tv);

        Button addCartButton = view.findViewById(R.id.cd_add_cart_btn);
        ImageView addQuantityImage = view.findViewById(R.id.cd_quantity_add_img);
        ImageView removeQuantityImage = view.findViewById(R.id.cd_quantity_remove_img);

        // formating the url to get a better thumbnail
        String urlDetail = NetworkUtils.formatUrlImages(
                mComic.getThumbnailURLInfo().getPath(),
                ImagesVariantsEnum.PORTRAIT_INCREDIBLE_256X324_PX.getValue(),
                mComic.getThumbnailURLInfo().getExtension()
        );
        // verify if the thumbnail is in the object; else recover from the server
        if (mComic.getThumbnailDetail() == null){
            new RetrieveImageTask(drawable -> {
                mComic.setThumbnailDetail(drawable);
                thumbnail.setImageDrawable(drawable);
                progressBar.setVisibility(View.INVISIBLE);
                loadingImageTextView.setVisibility(View.INVISIBLE);
            }).execute(urlDetail);
        }

        else {
            thumbnail.setImageDrawable(mComic.getThumbnailDetail());
            progressBar.setVisibility(View.INVISIBLE);
            loadingImageTextView.setVisibility(View.INVISIBLE);
        }

        priceTextView.setText(mComic.getPrice() > 0 ?
            String.format("$%.2f", mComic.getPrice()) : getString(R.string.free)
        );
        titleTextView.setText(mComic.getTitle());
        descriptionTextView.setText(mComic.getDescription() == null ?
                getString(R.string.no_description) : mComic.getDescription());

        if(descriptionTextView.getText().toString().contains("<br>")){
            descriptionTextView.setText(Html.fromHtml(descriptionTextView.getText().toString()));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            descriptionTextView.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }

        if (mComic.getLanguage().equals("en-US")) {
            languageTextView.setText(getResources().getString(R.string.english));
        } else {
            languageTextView.setText(getResources().getString(R.string.no_available));
        }

        authorsTextView.setText(mComic.getAuthors().isEmpty()?
             getString(R.string.no_available) : mComic.getAuthors()
        );
        formatTextView.setText(mComic.getFormat().isEmpty()?
            getString(R.string.no_available): mComic.getFormat()
        );

        if(mComic.getPageCount() == 0){
            pagesTitleTextView.setVisibility(View.INVISIBLE);
            pagesTextView.setVisibility(View.INVISIBLE);
        }
        else{
            pagesTextView.setText(String.format("%d pages", mComic.getPageCount()));
        }

        typeTextView.setText(mComic.isRare()? getString(R.string.rare): getString(R.string.common));

        int quantity = Integer.parseInt(quantityTextView.getText().toString());

        // add to cart button action
        addCartButton.setOnClickListener(view1 -> {
            // check if the item is in the cart
            if(CartItemRepository.getInstance().hasElement(mComic)){
                CartItemRepository.getInstance().update(mComic, quantity);
            }
            // add new item to cart
            else{
                CartItemRepository.getInstance()
                        .addItem(CartItemFactory.makeCartItem(mComic, quantity ));
            }
            // show message sucessful added to cart
            Toast toast = Toast.makeText(getContext(), getString(R.string.added_to_cart), Toast.LENGTH_SHORT);
            toast.show();
            this.dismiss();

        });

        addQuantityImage.setOnClickListener(view12 -> {
            int quant = Integer.parseInt(quantityTextView.getText().toString());
            quant++;
            quantityTextView.setText(String.format("%d", quant));
        });

        removeQuantityImage.setOnClickListener(view13 -> {
            int quant = Integer.parseInt(quantityTextView.getText().toString());
            if (quant > 1){
                quant--;
                quantityTextView.setText(String.format("%d", quant));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}