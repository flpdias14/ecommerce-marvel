package io.github.flpdias14.ecommercemarvel.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

import io.github.flpdias14.ecommercemarvel.R;

/**
 * Created by Felipe Oliveira on 10/10/2021
 */
public class LoadingDialog {

    private Activity mActivity;
    private AlertDialog mDialog;

    public LoadingDialog(Activity activity){
        this.mActivity = activity;
    }

    @SuppressLint("InflateParams")
    public void startLoadingDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater inflater = mActivity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.loading_dialog, null));
        builder.setCancelable(false);
        mDialog = builder.create();
        mDialog.show();
    }

    public void dimissDialog(){
        mDialog.dismiss();
    }
}
