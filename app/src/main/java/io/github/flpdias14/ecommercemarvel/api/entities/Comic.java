package io.github.flpdias14.ecommercemarvel.api.entities;


import java.util.List;

/**
 * Created by Felipe Oliveira on 08/10/2021
 */
public class Comic {

    private int id;
    private int digitalId;
    private String title;
    private double issueNumber;
    private String description;
    private String isbn;
    private String format;
    private int pageCount;
    private List<ComicPrice> prices;
    private Image thumbnail;
    private List<Image> images;
    private CreatorList creators;
//    private CharacterList characters;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public int getDigitalId() {
        return digitalId;
    }

    public double getIssueNumber() {
        return issueNumber;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getFormat() {
        return format;
    }

    public int getPageCount() {
        return pageCount;
    }

    public List<ComicPrice> getPrices() {
        return prices;
    }

    public Image getThumbnail() {
        return thumbnail;
    }

    public List<Image> getImages() {
        return images;
    }

    public CreatorList getCreators() {
        return creators;
    }

//    public CharacterList getCharacters() {
//        return characters;
//    }
}
